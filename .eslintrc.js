module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
  },
  'extends': [
    'google',
  ],
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaVersion': 13,
    'sourceType': 'module',
  },
  'plugins': [
    '@typescript-eslint',
    '@stylistic/js',
  ],
  'rules': {
    'object-curly-spacing': [1, 'always'],
    'quotes': ['error'],
    'semi': ['error', 'never'],
    'indent': ['error', 2, { 'ignoredNodes': ['PropertyDefinition'] }],
    'max-len': ['error', { 'code': 150 }],
    '@typescript-eslint/semi': 'off',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    // note you must disable the base rule as it can report incorrect errors
    'no-invalid-this': 'off',
    '@typescript-eslint/no-invalid-this': ['error'],
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': 'error',
    'require-jsdoc': 'off',
    'new-cap': 'off',
    'no-multi-spaces': 'error',
    'sort-imports': [
      'error',
      {
        'ignoreCase': true,
        'ignoreDeclarationSort': true,
      },
    ],
  },
};

module.exports = {
  roots: ['<rootDir>/'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  testRegex: '\\.spec\\.ts$',
  testPathIgnorePatterns: ['<rootDir>//node_modules'],
  setupFiles: ['<rootDir>//test/setup.ts'],
  resetMocks: false,
  moduleNameMapper: {
    '@/(.*)': '<rootDir>/src/$1',
    '@api/(.*)': '<rootDir>/src/api/$1',
    '@entities/(.*)': '<rootDir>/src/entities/$1',
    '@models/(.*)': '<rootDir>/src/models/$1',
    '@application/(.*)': '<rootDir>/src/application/$1',
  },
}

export const testJobFactory = {
  id: 1,
  contractID: 2,
  description: 'test jobs',
  price: 500,
  paid: false,
  paymentDate: new Date(),
  contract: {
    client: {
      balance: 9000,
    },
  },
}

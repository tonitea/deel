import { Status } from '@entities/contract/contract.entity'

export const testContractFactory = {
  id: 1,
  contractorID: 2,
  clientID: 3,
  terms: 'fake terms',
  status: Status.IN_PROGRESS,
}
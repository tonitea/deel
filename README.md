# Cookie cutter NestJS + Sequelize by Antonio Azcoita


## Description

Let's follow this guide in order to test the app.

## Improvements

After confirming with Julien whether would be possible to implement the solution in Typescript(TS),
instead of plain JS + NodeJS, decided to migrate it using TS + NestJS framework. In my honest opinion,
it is a great backed tool although it required more work than expected :)

## Installation

1. Install dependencies from root path

```bash
$ yarn
```

2. Running the app

```bash
$ yarn start:dev
```

Once the app is run, the database named ``database.sqlite3`` in the parent root path. It will create
all the tables automatically, because of ``Sequelize`` sync.

3. Seeding data

After running the app with the purpose of creating the database structure, lets run the migration 
for adding the data seed.

```bash
$ yarn migrate:up
```

It will add the seeds stored in the migration ``src/infrastructure/migrations/20240501191009-seeding-data.jsx``.

## Test

In order to run unit tests.

```bash
# unit tests
$ yarn  test
```

## Postman

In order to test each endpoint created, shared the workspace as well as the 
postman environment, created during development under `test/postman` folder.

# Repository

Aside attaching the repo in the email sent to Julien, I created a repo within my Gitlab
account, including all needed:

https://gitlab.com/tonitea/cookie-cutter-nesjs-sequelize

## Future improvements

In this exercise did not include some other ideas are good practices, I usually work in daily:

- Create integrations or e2e tests (aside unit tests), using some library, such as ``supertest``.
- Add payload validation using some library such as ``joi`` or ``zod``. My personal preference
when working with TS is to use zod :)

## Stay in touch

- Author - [Antonio Azcoita](antonio.azcoita@gmail.com)

## License


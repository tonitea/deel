import { ProfileEntity } from '@entities/profile/profile.entity'
import { PROFILE_REPOSITORY } from '@/utils/constants'

export const profileProviders = [
  {
    provide: PROFILE_REPOSITORY,
    useValue: ProfileEntity,
  },
]

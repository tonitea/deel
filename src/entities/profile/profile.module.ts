import { Module } from '@nestjs/common'
import { profileProviders } from '@entities/profile/profile.provider'
import { ProfileEntity } from '@entities/profile/profile.entity'
import { SequelizeModule } from '@nestjs/sequelize'

@Module({
  imports: [SequelizeModule.forFeature([ProfileEntity])],
  exports: [ProfileEntity, ...profileProviders, SequelizeModule],
  providers: [ProfileEntity, ...profileProviders, SequelizeModule],
})
export class ProfileModule {}

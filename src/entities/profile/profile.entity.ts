import {
  AllowNull,
  AutoIncrement,
  Column,
  DataType,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript'
import { PROFILE_TABLE_NAME } from '@/utils/constants'

export enum ProfileType {
  CLIENT = 'client',
  CONTRACTOR = 'contractor',
}

@Table({
  tableName: PROFILE_TABLE_NAME,
  timestamps: true,
})
export class ProfileEntity extends Model {
  @PrimaryKey
  @AllowNull(false)
  @AutoIncrement
  @Column(DataType.INTEGER)
  id!: number

  @AllowNull(false)
  @Column(DataType.TEXT)
  firstName!: string

  @AllowNull(false)
  @Column(DataType.TEXT)
  lastName!: string

  @AllowNull(false)
  @Column(DataType.TEXT)
  profession!: string

  @Column(DataType.DECIMAL(12, 2))
  balance: number

  @AllowNull(false)
  @Column(DataType.ENUM(...Object.values(ProfileType)))
  type: ProfileType
}

import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript'

import { ProfileEntity } from '@entities/profile/profile.entity'
import { CONTRACTS_TABLE_NAME } from '@/utils/constants'

export enum Status {
  NEW = 'new',
  IN_PROGRESS = 'in_progress',
  TERMINATED = 'terminated',
}

@Table({
  tableName: CONTRACTS_TABLE_NAME,
  timestamps: true,
})
export class ContractEntity extends Model {
  @AllowNull(false)
  @AutoIncrement
  @PrimaryKey
  @Column(DataType.INTEGER)
  id!: number

  @ForeignKey(() => ProfileEntity)
  @Column(DataType.INTEGER)
  contractorID!: number

  @BelongsTo(() => ProfileEntity, {
    foreignKey: 'contractorID',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  contractor: ProfileEntity

  @ForeignKey(() => ProfileEntity)
  @Column(DataType.INTEGER)
  clientID!: number

  @BelongsTo(() => ProfileEntity, {
    foreignKey: 'clientID',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  client: ProfileEntity

  @AllowNull(false)
  @Column(DataType.TEXT)
  terms!: string

  @AllowNull(false)
  @Column(DataType.ENUM(...Object.values(Status)))
  status!: Status
}

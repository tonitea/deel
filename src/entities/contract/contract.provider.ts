import { ContractEntity } from '@entities/contract/contract.entity'
import { CONTRACT_REPOSITORY } from '@/utils/constants'

export const contractProviders = [
  {
    provide: CONTRACT_REPOSITORY,
    useValue: ContractEntity,
  },
]

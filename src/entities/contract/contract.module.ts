import { Module } from '@nestjs/common'
import { contractProviders } from '@entities/contract/contract.provider'
import { ContractEntity } from '@entities/contract/contract.entity'
import { SequelizeModule } from '@nestjs/sequelize'

@Module({
  imports: [SequelizeModule.forFeature([ContractEntity])],
  exports: [ContractEntity, ...contractProviders, SequelizeModule],
  providers: [ContractEntity, ...contractProviders, SequelizeModule],
})
export class ContractModule {}

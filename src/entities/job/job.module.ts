import { Module } from '@nestjs/common'
import { jobProviders } from '@entities/job/job.provider'
import { JobEntity } from '@entities/job/job.entity'
import { SequelizeModule } from '@nestjs/sequelize'

@Module({
  imports: [SequelizeModule.forFeature([JobEntity])],
  exports: [JobEntity, ...jobProviders, SequelizeModule],
  providers: [JobEntity, ...jobProviders, SequelizeModule],
})
export class JobModule {}

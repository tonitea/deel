import { JobEntity } from '@entities/job/job.entity'
import { JOB_REPOSITORY } from '@/utils/constants'

export const jobProviders = [
  {
    provide: JOB_REPOSITORY,
    useValue: JobEntity,
  },
]

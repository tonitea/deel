import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  Default,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript'

import { JOBS_TABLE_NAME } from '@/utils/constants'
import { ContractEntity } from '@entities/contract/contract.entity'

@Table({
  tableName: JOBS_TABLE_NAME,
  timestamps: true,
})
export class JobEntity extends Model {
  @PrimaryKey
  @AllowNull(false)
  @AutoIncrement
  @Column(DataType.INTEGER)
  id!: number

  @ForeignKey(() => ContractEntity)
  @Column(DataType.INTEGER)
  contractID!: number

  @BelongsTo(() => ContractEntity, {
    foreignKey: 'contractID',
    targetKey: 'id',
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  contract: ContractEntity

  @AllowNull(false)
  @Column(DataType.TEXT)
  description!: string

  @AllowNull(false)
  @Column(DataType.DECIMAL(12, 2))
  price!: number

  @AllowNull(false)
  @Default(false)
  @Column(DataType.BOOLEAN)
  paid!: boolean

  @Column(DataType.DATE)
  paymentDate: Date
}

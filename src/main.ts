import { NestFactory } from '@nestjs/core'
import { VersioningType } from '@nestjs/common'
import { MainModule } from '@/main.module'
import { SwaggerModule } from '@nestjs/swagger'
import { swaggerConfig } from '@/utils/swaggerConfig'
import { API_VERSIONING } from '@/utils/constants'

async function bootstrap() {
  const app = await NestFactory.create(MainModule, {
    logger: ['log', 'debug', 'warn', 'error'],
  })

  app.setGlobalPrefix('api')

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: API_VERSIONING,
  })

  const document = SwaggerModule.createDocument(app, swaggerConfig)
  SwaggerModule.setup('api-docs', app, document)

  await app.listen(3001)
}
bootstrap()

import { MiddlewareConsumer, Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import config from './utils/config'
import { ApiModule } from '@api/api.module'
import { ApplicationModule } from '@/application/application.module'
import { InfrastructureModule } from './infrastructure/infrastructure.module'
import { ProfileMiddleware } from '@api/middlewares/profile.middleware'
import { ContractController } from '@api/controllers/contracts.controller'
import { JobController } from '@api/controllers/jobs.controller'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    ApiModule,
    ApplicationModule,
    InfrastructureModule,
  ],
})
export class MainModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(ProfileMiddleware)
      .forRoutes(ContractController, JobController)
  }
}

import { AppConfiguration } from './config.model'

export default function Config(): AppConfiguration {
  try {
    return {
      database: {},
    }
  } catch (err) {
    throw new Error(err)
  }
}

import { DocumentBuilder } from '@nestjs/swagger'

export const swaggerConfig = new DocumentBuilder()
  .setTitle('Cookie cutter NestJS + Sequelize')
  .setDescription('created by Antonio Azcoita')
  .setVersion('1.0')
  .build()

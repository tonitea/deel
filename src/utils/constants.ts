export const CONTRACTS_TABLE_NAME = 'contract'
export const JOBS_TABLE_NAME = 'job'
export const PROFILE_TABLE_NAME = 'profile'

export const CONTRACT_REPOSITORY = 'CONTRACT_REPOSITORY'
export const JOB_REPOSITORY = 'JOB_REPOSITORY'
export const PROFILE_REPOSITORY = 'PROFILE_REPOSITORY'

export const DATABASE_NAME = 'database.sqlite3'

export const API_VERSIONING = '1'

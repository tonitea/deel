import { HttpStatus, Injectable, Logger, NestMiddleware } from '@nestjs/common'
import { NextFunction, Request, Response } from 'express'
import { ProfileRepository } from '@/infrastructure/repositories/profile.repository'

@Injectable()
export class ProfileMiddleware implements NestMiddleware {
  constructor(private readonly profileRepository: ProfileRepository) {}

  private readonly logger = new Logger(ProfileMiddleware.name)

  async use(req: Request, res: Response, next: NextFunction) {
    this.logger.log('Calling profile middleware...')

    const profile = await this.profileRepository.findOne(
      +(req.headers['profile_id'] ?? 0),
    )

    if (!profile) {
      return res.status(HttpStatus.UNAUTHORIZED).end()
    }

    req.body = profile

    next()
  }
}

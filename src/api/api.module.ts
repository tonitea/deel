import { Module } from '@nestjs/common'
import { MiddlewaresModule } from './middlewares/middlewares.module'
import { ControllersModule } from './controllers/controllers.module'

@Module({
  imports: [MiddlewaresModule, ControllersModule],
})
export class ApiModule {}

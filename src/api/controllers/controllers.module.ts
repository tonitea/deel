import { Module } from '@nestjs/common'
import { ContractController } from '@api/controllers/contracts.controller'
import { CqrsModule } from '@nestjs/cqrs'
import { JobController } from '@api/controllers/jobs.controller'
import { BalancesController } from '@api/controllers/balances.controller'
import { AdminController } from '@api/controllers/admin.controller'

@Module({
  imports: [CqrsModule],
  controllers: [
    ContractController,
    JobController,
    BalancesController,
    AdminController,
  ],
})
export class ControllersModule {}

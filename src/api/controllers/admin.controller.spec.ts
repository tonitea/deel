import { Test, TestingModule } from '@nestjs/testing'
import { CqrsModule } from '@nestjs/cqrs'
import { AdminController } from '@api/controllers/admin.controller'

describe('AdminController', () => {
  let controller: AdminController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdminController],
      imports: [CqrsModule],
    }).compile()

    controller = module.get<AdminController>(AdminController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})

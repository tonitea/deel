import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  Query,
} from '@nestjs/common'
import { ApiOkResponse, ApiTags } from '@nestjs/swagger'
import { QueryBus } from '@nestjs/cqrs'
import { GetBestProfessionQuery } from '@/application/useCases/getBestProfessionUseCase/getBestProfession.query'
import { GetBestClientsQuery } from '@/application/useCases/getBestClientsUseCase/getBestClients.query'

@ApiTags('Admin Controller')
@Controller('admin')
export class AdminController {
  constructor(private readonly mediator: QueryBus) {}

  private readonly logger = new Logger(AdminController.name)

  @Get('best-profession')
  @HttpCode(HttpStatus.OK)
  @HttpCode(HttpStatus.BAD_REQUEST)
  @HttpCode(HttpStatus.INTERNAL_SERVER_ERROR)
  @HttpCode(HttpStatus.UNAUTHORIZED)
  @ApiOkResponse({
    description:
      'Returns the profession that earned the most money (sum of jobs paid) for any contactor that worked in the query time range.',
  })
  async getBestProfession(
    @Query('start') start: Date,
    @Query('end') end: Date,
  ): Promise<void> {
    this.logger.log(`Get best profession for interval: ${start} - ${end}`)

    const query = new GetBestProfessionQuery(start, end)

    return await this.mediator.execute(query)
  }

  @Get('best-clients')
  @HttpCode(HttpStatus.OK)
  @HttpCode(HttpStatus.BAD_REQUEST)
  @HttpCode(HttpStatus.INTERNAL_SERVER_ERROR)
  @HttpCode(HttpStatus.UNAUTHORIZED)
  @ApiOkResponse({
    description:
      'Returns the clients the paid the most for jobs in the query time period. limit query parameter should be applied, default limit is 2..',
  })
  async getBestClients(
    @Query('start') start: Date,
    @Query('end') end: Date,
    @Query('limit') limit: number,
  ): Promise<void> {
    this.logger.log(`Get best clients for interval: ${start} - ${end} using the limit provied`)

    const query = new GetBestClientsQuery(start, end, limit)

    return await this.mediator.execute(query)
  }
}

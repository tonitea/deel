import { Test, TestingModule } from '@nestjs/testing'
import { ContractController } from '@api/controllers/contracts.controller'
import { CqrsModule } from '@nestjs/cqrs'

describe('ContractsController', () => {
  let controller: ContractController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ContractController],
      imports: [CqrsModule],
    }).compile()

    controller = module.get<ContractController>(ContractController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})

import { Test, TestingModule } from '@nestjs/testing'
import { BalancesController } from '@api/controllers/balances.controller'
import { CqrsModule } from '@nestjs/cqrs'

describe('BalancesController', () => {
  let controller: BalancesController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BalancesController],
      imports: [CqrsModule],
    }).compile()

    controller = module.get<BalancesController>(BalancesController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})

import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  Param,
  Post,
} from '@nestjs/common'
import { ApiOkResponse, ApiTags } from '@nestjs/swagger'
import { CommandBus, QueryBus } from '@nestjs/cqrs'
import { GetUnpaidJobsQuery } from '@/application/useCases/getUnpaidJobsUseCase/getUnpaidJobs.query'
import { ProfileEntity } from '@entities/profile/profile.entity'
import { JobEntity } from '@entities/job/job.entity'
import { PayJobCommand } from '@/application/useCases/payJobUseCase/payJob.command'

@ApiTags('Jobs Controller')
@Controller('jobs')
export class JobController {
  constructor(
    private readonly mediatorQuery: QueryBus,
    private readonly mediatorCommand: CommandBus,
  ) {}

  private readonly logger = new Logger(JobController.name)

  @Get('/unpaid')
  @HttpCode(HttpStatus.OK)
  @HttpCode(HttpStatus.BAD_REQUEST)
  @HttpCode(HttpStatus.INTERNAL_SERVER_ERROR)
  @HttpCode(HttpStatus.UNAUTHORIZED)
  @ApiOkResponse({
    description:
      'Get all unpaid jobs for a user (either a client or contractor), for active contracts only',
  })
  async getUnpaidJobs(@Body() profile: ProfileEntity): Promise<JobEntity> {
    this.logger.log('Getting unpaid jobs for profile:', profile.id)

    const query = new GetUnpaidJobsQuery(profile.id)

    return await this.mediatorQuery.execute(query)
  }

  @Post('/:jobID/pay')
  @HttpCode(HttpStatus.OK)
  @HttpCode(HttpStatus.BAD_REQUEST)
  @HttpCode(HttpStatus.INTERNAL_SERVER_ERROR)
  @HttpCode(HttpStatus.UNAUTHORIZED)
  @ApiOkResponse({
    description:
      'Pay for a job, a client can only pay if his balance >= the amount to pay. ' +
      'The amount should be moved from the clients balance to the contractor balance.',
  })
  async payJob(
    @Body() profile: ProfileEntity,
    @Param('jobID') jobID: number,
  ): Promise<void> {
    this.logger.log('Start to pay job:', jobID)

    const command = new PayJobCommand(profile.id, jobID)

    return this.mediatorCommand.execute(command)
  }
}

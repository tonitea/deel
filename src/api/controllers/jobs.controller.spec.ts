import { Test, TestingModule } from '@nestjs/testing'
import { JobController } from '@api/controllers/jobs.controller'
import { CqrsModule } from '@nestjs/cqrs'

describe('JobController', () => {
  let controller: JobController

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JobController],
      imports: [CqrsModule],
    }).compile()

    controller = module.get<JobController>(JobController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})

import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Logger,
  Param,
} from '@nestjs/common'
import { ApiOkResponse, ApiTags } from '@nestjs/swagger'
import { QueryBus } from '@nestjs/cqrs'
import { ContractEntity } from '@entities/contract/contract.entity'
import { GetContractsByProfileQuery } from '@/application/useCases/getContractsByProfileUseCase/getContractsByProfile.query'
import { ProfileEntity } from '@entities/profile/profile.entity'
import { GetContractByIDQuery } from '@/application/useCases/getContractByIDUseCase/getContractByID.query'

@ApiTags('Contracts Controller')
@Controller('contracts')
export class ContractController {
  constructor(private readonly mediator: QueryBus) {}

  private readonly logger = new Logger(ContractController.name)

  @Get()
  @HttpCode(HttpStatus.OK)
  @HttpCode(HttpStatus.BAD_REQUEST)
  @HttpCode(HttpStatus.INTERNAL_SERVER_ERROR)
  @HttpCode(HttpStatus.UNAUTHORIZED)
  @ApiOkResponse({
    description:
      'Returns a list of contracts belonging to a user (client or contractor), the list should only contain non terminated contracts',
  })
  async getContractsByProfile(
    @Body() profile: ProfileEntity,
  ): Promise<ContractEntity[]> {
    this.logger.log('Getting contracts for profile:', profile)

    const query = new GetContractsByProfileQuery(profile.id)

    return await this.mediator.execute(query)
  }

  @Get('/:contractID')
  @HttpCode(HttpStatus.OK)
  @HttpCode(HttpStatus.BAD_REQUEST)
  @HttpCode(HttpStatus.INTERNAL_SERVER_ERROR)
  @HttpCode(HttpStatus.UNAUTHORIZED)
  @ApiOkResponse({
    description:
      'Returns the contract information ny its ID, just in case the user profile is authorized',
  })
  async getContractByID(
    @Body() profile: ProfileEntity,
    @Param('contractID') contractID: number,
  ): Promise<ContractEntity> {
    this.logger.log('Getting contracts by ID:', contractID)

    const query = new GetContractByIDQuery(profile.id, contractID)

    return await this.mediator.execute(query)
  }
}

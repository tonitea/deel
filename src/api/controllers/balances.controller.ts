import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Logger,
  Param,
  Post,
} from '@nestjs/common'
import { ApiOkResponse, ApiTags } from '@nestjs/swagger'
import { CommandBus } from '@nestjs/cqrs'
import { AddBalanceRequest } from '@models/addBalance.request'
import { AddCreditRequestCommand } from '@/application/useCases/addCreditRequestUseCase/addCreditRequest.command'

@ApiTags('Balances Controller')
@Controller('balances')
export class BalancesController {
  constructor(private readonly mediator: CommandBus) {}

  private readonly logger = new Logger(BalancesController.name)

  @Post('/deposit/:clientID')
  @HttpCode(HttpStatus.OK)
  @HttpCode(HttpStatus.BAD_REQUEST)
  @HttpCode(HttpStatus.INTERNAL_SERVER_ERROR)
  @HttpCode(HttpStatus.UNAUTHORIZED)
  @ApiOkResponse({
    description:
      'Deposits money into the the the balance of a client, a client cant deposit more than 25% his total of jobs to pay. (at the deposit moment)',
  })
  async addCreditClient(
    @Param('clientID') clientID: number,
    @Body() balanceRequest: AddBalanceRequest,
  ): Promise<void> {
    this.logger.log(`Add credit request for client ${clientID} 
    with new credit: ${JSON.stringify(balanceRequest)}`)

    const command = new AddCreditRequestCommand(balanceRequest.credit, clientID)

    return await this.mediator.execute(command)
  }
}

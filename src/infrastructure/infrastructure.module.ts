import { Module } from '@nestjs/common'
import { databaseProviders } from '@/infrastructure/database.provider'
import { ContractRepository } from '@/infrastructure/repositories/contract.repository'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { ProfileRepository } from '@/infrastructure/repositories/profile.repository'
import { JobModule } from '@entities/job/job.module'
import { ContractModule } from '@entities/contract/contract.module'
import { ProfileModule } from '@entities/profile/profile.module'
import { SequelizeModule } from '@nestjs/sequelize'
import { DATABASE_NAME } from '@/utils/constants'
import { ContractEntity } from '@entities/contract/contract.entity'
import { JobEntity } from '@entities/job/job.entity'
import { ProfileEntity } from '@entities/profile/profile.entity'

const repositories = [ContractRepository, JobRepository, ProfileRepository]
const entities = [ContractEntity, JobEntity, ProfileEntity]
const modules = [ContractModule, JobModule, ProfileModule]

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'sqlite',
      database: DATABASE_NAME,
      storage: `../${DATABASE_NAME}`,
      autoLoadModels: true,
      synchronize: true,
      models: [...entities],
    }),
    ...modules,
  ],
  exports: [...databaseProviders, ...repositories],
  providers: [...databaseProviders, ...repositories],
})
export class InfrastructureModule {}

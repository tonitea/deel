import { Inject, Injectable } from '@nestjs/common'
import { JobEntity } from '@entities/job/job.entity'
import { JOB_REPOSITORY } from '@/utils/constants'
import sequelize, { Op } from 'sequelize'
import { ContractEntity, Status } from '@entities/contract/contract.entity'

@Injectable()
export class JobRepository {
  constructor(
    @Inject(JOB_REPOSITORY)
    private readonly job: typeof JobEntity,
  ) {}

  async findUnpaid(profileID: number): Promise<JobEntity[]> {
    return this.job.findAll({
      include: [
        {
          association: 'contract',
          // INNER JOIN
          required: true,
          where: {
            [Op.and]: [
              { [Op.or]: { contractorID: profileID, clientID: profileID } },
              { [Op.not]: { status: Status.TERMINATED } },
            ],
          },
        },
      ],
      where: {
        paid: false,
      },
    })
  }

  async searchJob(profileID: number, jobID: number): Promise<any> {
    // Type it with as `any` to reflect the JOIN typing
    return await this.job.findOne({
      include: [
        {
          model: ContractEntity,
          as: 'contract',
          // INNER JOIN
          required: true,
          include: [
            {
              association: 'client',
              // INNER JOIN
              required: true,
              where: { id: profileID },
            },
          ],
        },
      ],
      where: {
        [Op.and]: [{ id: jobID }, { paid: false }],
      },
    })
  }

  async payJob(jobID: number): Promise<void> {
    await this.job.update({ paid: true }, { where: { id: jobID } })

    return
  }

  async getTotalToPayForClient(clientID: number): Promise<any> {
    return this.job.findOne({
      include: [
        {
          model: ContractEntity,
          as: 'contract',
          // INNER JOIN
          required: true,
          where: { clientID },
          attributes: [
            [
              sequelize.fn('sum', sequelize.col('price')),
              'total_pending_payment',
            ],
          ],
        },
      ],
      where: { paid: false },
      raw: true,
    })
  }

  async getBestPaidProfessionForInterval(start: Date, end: Date): Promise<any> {
    return this.job.findOne({
      include: [
        {
          model: ContractEntity,
          as: 'contract',
          // INNER JOIN
          required: true,
          include: [
            {
              association: 'contractor',
              // INNER JOIN
              required: true,
              attributes: ['profession'],
            },
          ],
        },
      ],
      group: 'contract.contractorID',
      where: {
        [Op.and]: [
          { paymentDate: { [Op.between]: [start, end] } },
          { paid: true },
        ],
      },
      attributes: [
        [sequelize.fn('sum', sequelize.col('price')), 'total_paid'],
      ],
      order: [['createdAt', 'DESC']],
      limit: 1,
    })
  }

  async getClientsPaidMostForInterval(start: Date, end: Date, limit: number = 2): Promise<any> {
    return this.job.findOne({
      include: [
        {
          model: ContractEntity,
          as: 'contract',
          // INNER JOIN
          required: true,
          include: [
            {
              association: 'client',
              // INNER JOIN
              required: true,
            },
          ],
        },
      ],
      group: 'contract.clientID',
      where: {
        [Op.and]: [
          { paymentDate: { [Op.between]: [start, end] } },
          { paid: true },
        ],
      },
      attributes: [
        [sequelize.fn('sum', sequelize.col('price')), 'total_paid'],
      ],
      order: [['createdAt', 'DESC']],
      limit,
    })
  }
}

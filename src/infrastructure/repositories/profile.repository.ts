import { Inject, Injectable } from '@nestjs/common'
import { ProfileEntity } from '@entities/profile/profile.entity'
import { PROFILE_REPOSITORY } from '@/utils/constants'

@Injectable()
export class ProfileRepository {
  constructor(
    @Inject(PROFILE_REPOSITORY)
    private readonly profile: typeof ProfileEntity,
  ) {}

  async findOne(id: number): Promise<ProfileEntity | null> {
    return this.profile.findOne({ where: { id } })
  }

  async updateBalance(profileID: number, balance: number): Promise<void> {
    await this.profile.update(
      {
        balance,
        paymentDate: new Date(),
      },
      { where: { id: profileID } },
    )

    return
  }

  async incrementClientBalance(
    clientID: number,
    balanceInc: number,
  ): Promise<void> {
    await this.profile.increment('balance', {
      where: { id: clientID },
      by: balanceInc,
    })

    return
  }
}

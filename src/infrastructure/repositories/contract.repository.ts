import { Inject, Injectable } from '@nestjs/common'
import { Op } from 'sequelize'
import { ContractEntity, Status } from '@entities/contract/contract.entity'
import { CONTRACT_REPOSITORY } from '@/utils/constants'

@Injectable()
export class ContractRepository {
  constructor(
    @Inject(CONTRACT_REPOSITORY)
    private readonly contract: typeof ContractEntity,
  ) {}

  async findByProfileID(profileID: number): Promise<ContractEntity[]> {
    return this.contract.findAll({
      where: {
        [Op.and]: [
          { [Op.or]: { contractorID: profileID, clientID: profileID } },
          { [Op.not]: { status: Status.TERMINATED } },
        ],
      },
    })
  }

  async findByID(
    profileID: number,
    contractID: number,
  ): Promise<ContractEntity[]> {
    return this.contract.findAll({
      where: {
        [Op.and]: [
          { [Op.or]: { contractorID: profileID, clientID: profileID } },
          { id: contractID },
        ],
      },
    })
  }
}

module.exports = {
  'development': {
    'dialect': 'sqlite',
    'name': 'database.sqlite3',
    'storage': '../database.sqlite3',
    'autoLoadModels': true,
    'synchronize': true,
  },
  'test': {
    'dialect': 'sqlite',
    'name': 'database.sqlite3',
    'storage': '../database.sqlite3',
    'autoLoadModels': true,
    'synchronize': true,
  },
  'production': {
    'dialect': 'sqlite',
    'name': 'database.sqlite3',
    'storage': '../$database.sqlite3',
    'autoLoadModels': true,
    'synchronize': true,
  },
}

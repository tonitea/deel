import { Sequelize } from 'sequelize-typescript'
import { ContractEntity } from '@entities/contract/contract.entity'
import { JobEntity } from '@entities/job/job.entity'
import { ProfileEntity } from '@entities/profile/profile.entity'
import { DATABASE_NAME } from '@/utils/constants'

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'sqlite',
        storage: `../${DATABASE_NAME}`,
      })

      sequelize.addModels([ContractEntity, JobEntity, ProfileEntity])
      await sequelize.sync()

      return sequelize
    },
  },
]

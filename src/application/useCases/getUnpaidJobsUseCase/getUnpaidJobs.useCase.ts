import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { BadRequestException, Logger } from '@nestjs/common'
import { GetUnpaidJobsQuery } from '@/application/useCases/getUnpaidJobsUseCase/getUnpaidJobs.query'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { JobEntity } from '@entities/job/job.entity'

@QueryHandler(GetUnpaidJobsQuery)
export class GetUnpaidJobsUseCase implements IQueryHandler<GetUnpaidJobsQuery> {
  constructor(private readonly jobRepository: JobRepository) {}

  private readonly logger = new Logger(GetUnpaidJobsUseCase.name)

  async execute(query: GetUnpaidJobsQuery): Promise<JobEntity[]> {
    this.logger.log(`GetUnpaidJobsUseCase for profile: ${query.profileID}`)

    if (!query.profileID) {
      throw new BadRequestException()
    }

    return await this.jobRepository.findUnpaid(query.profileID)
  }
}

import { createMock } from '@golevelup/ts-jest'
import { GetUnpaidJobsUseCase } from '@/application/useCases/getUnpaidJobsUseCase/getUnpaidJobs.useCase'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { GetUnpaidJobsQuery } from '@/application/useCases/getUnpaidJobsUseCase/getUnpaidJobs.query'
import { testJobFactory } from '../../../../test/factories/testJob.factory'

describe('GetUnpaidJobsUseCase', () => {
  let sut: GetUnpaidJobsUseCase

  const profileID = 1

  const mockJobRepository = createMock<JobRepository>({
    findUnpaid: jest.fn().mockResolvedValue([testJobFactory, testJobFactory]),
  })

  beforeEach(() => {
    jest.clearAllMocks()

    sut = new GetUnpaidJobsUseCase(mockJobRepository)
  })

  describe('execute', () => {
    it('should get unpaid jobs for a profile', async () => {
      // ARRANGE
      const query = new GetUnpaidJobsQuery(profileID)

      // ACT
      const result = await sut.execute(query)

      // ASSERT
      expect(mockJobRepository.findUnpaid).toHaveBeenCalledWith(profileID)
      expect(result).toStrictEqual([testJobFactory, testJobFactory])
    })
  })
})

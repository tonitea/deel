import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { BadRequestException, Logger } from '@nestjs/common'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { GetBestClientsQuery } from '@/application/useCases/getBestClientsUseCase/getBestClients.query'

@QueryHandler(GetBestClientsQuery)
export class GetBestClientsUseCase
implements IQueryHandler<GetBestClientsQuery> {
  constructor(private readonly jobRepository: JobRepository) {}

  private readonly logger = new Logger(GetBestClientsUseCase.name)

  async execute(query: GetBestClientsQuery): Promise<void> {
    this.logger.log(
      `GetBestClientsUseCase for interval: ${query.start} - ${query.end} & limit: ${query.limit}`,
    )

    if (!query.start || !query.end) {
      throw new BadRequestException('Invalid start or end of range')
    }

    return await this.jobRepository.getClientsPaidMostForInterval(
      query.start,
      query.end,
      query.limit,
    )
  }
}

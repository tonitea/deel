export class GetBestClientsQuery {
  constructor(
    public readonly start: Date,
    public readonly end: Date,
    public readonly limit?: number,
  ) {}
}

import { createMock } from '@golevelup/ts-jest'
import { testContractFactory } from '../../../../test/factories/testContract.factory'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { GetBestClientsUseCase } from '@/application/useCases/getBestClientsUseCase/getBestClients.useCase'
import { GetBestClientsQuery } from '@/application/useCases/getBestClientsUseCase/getBestClients.query'

describe('GetBestClientsUseCase', () => {
  let sut: GetBestClientsUseCase
  const startDate = new Date('2020-07-14T19:11:26.737Z')
  const endDate = new Date('2020-07-15T19:11:26.737Z')
  const limit = 1

  const mockJobRepository = createMock<JobRepository>({
    getClientsPaidMostForInterval: jest.fn().mockResolvedValue(testContractFactory),
  })

  beforeEach(() => {
    jest.clearAllMocks()

    sut = new GetBestClientsUseCase(mockJobRepository)
  })

  describe('execute', () => {
    it('should get best clients properly with limit', async () => {
      // ARRANGE
      const query = new GetBestClientsQuery(startDate, endDate, limit)

      // ACT
      await sut.execute(query)

      // ASSERT
      expect(mockJobRepository.getClientsPaidMostForInterval).toHaveBeenCalledWith(startDate, endDate, limit)
    })

    it('should get best clients properly without limit', async () => {
      // ARRANGE
      const query = new GetBestClientsQuery(startDate, endDate)

      // ACT
      await sut.execute(query)

      // ASSERT
      expect(mockJobRepository.getClientsPaidMostForInterval).toHaveBeenCalledWith(startDate, endDate, undefined)
    })
  })
})

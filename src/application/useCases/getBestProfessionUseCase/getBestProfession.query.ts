export class GetBestProfessionQuery {
  constructor(
    public readonly start: Date,
    public readonly end: Date,
  ) {}
}

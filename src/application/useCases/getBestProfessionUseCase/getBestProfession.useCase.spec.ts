import { createMock } from '@golevelup/ts-jest'
import { testContractFactory } from '../../../../test/factories/testContract.factory'
import { GetBestProfessionUseCase } from '@/application/useCases/getBestProfessionUseCase/GetBestProfession.useCase'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { GetBestProfessionQuery } from '@/application/useCases/getBestProfessionUseCase/getBestProfession.query'

describe('GetBestProfessionUseCase', () => {
  let sut: GetBestProfessionUseCase
  const startDate = new Date('2020-07-14T19:11:26.737Z')
  const endDate = new Date('2020-07-15T19:11:26.737Z')

  const mockJobRepository = createMock<JobRepository>({
    getBestPaidProfessionForInterval: jest.fn().mockResolvedValue(testContractFactory),
  })

  beforeEach(() => {
    jest.clearAllMocks()

    sut = new GetBestProfessionUseCase(mockJobRepository)
  })

  describe('execute', () => {
    it('should get best professional properly', async () => {
      // ARRANGE
      const query = new GetBestProfessionQuery(startDate, endDate)

      // ACT
      await sut.execute(query)

      // ASSERT
      expect(mockJobRepository.getBestPaidProfessionForInterval).toHaveBeenCalledWith(startDate, endDate)
    })
  })
})

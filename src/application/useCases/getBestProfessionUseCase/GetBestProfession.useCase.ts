import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { BadRequestException, Logger } from '@nestjs/common'
import { GetBestProfessionQuery } from '@/application/useCases/getBestProfessionUseCase/getBestProfession.query'
import { JobRepository } from '@/infrastructure/repositories/job.repository'

@QueryHandler(GetBestProfessionQuery)
export class GetBestProfessionUseCase
implements IQueryHandler<GetBestProfessionQuery> {
  constructor(private readonly jobRepository: JobRepository) {}

  private readonly logger = new Logger(GetBestProfessionUseCase.name)

  async execute(query: GetBestProfessionQuery): Promise<void> {
    this.logger.log(
      `GetBestProfessionUseCase for interval: ${query.start} - ${query.end}`,
    )

    if (!query.start || !query.end) {
      throw new BadRequestException('Invalid start or end of range')
    }

    return await this.jobRepository.getBestPaidProfessionForInterval(
      query.start,
      query.end,
    )
  }
}

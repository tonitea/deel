import { ICommand } from '@nestjs/cqrs'

export class AddCreditRequestCommand implements ICommand {
  constructor(
    public readonly credit: number,
    public readonly clientID: number,
  ) {}
}

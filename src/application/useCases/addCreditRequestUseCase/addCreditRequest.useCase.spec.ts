import { AddCreditRequestUseCase } from '@/application/useCases/addCreditRequestUseCase/addCreditRequest.useCase'
import { createMock } from '@golevelup/ts-jest'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { ProfileRepository } from '@/infrastructure/repositories/profile.repository'
import { AddCreditRequestCommand } from '@/application/useCases/addCreditRequestUseCase/addCreditRequest.command'
import { BadRequestException } from '@nestjs/common'

describe('AddCreditRequestUseCase', () => {
  let sut: AddCreditRequestUseCase

  const clientID = 1
  const credit = 50

  const mockJobRepository = createMock<JobRepository>({
    getTotalToPayForClient: jest.fn().mockResolvedValue({
      'contract.total_pending_payment': 400,
    }),
  })
  const mockProfileRepository = createMock<ProfileRepository>()

  beforeEach(() => {
    jest.clearAllMocks()

    sut = new AddCreditRequestUseCase(mockJobRepository, mockProfileRepository)
  })

  describe('execute', () => {
    it('should add credit successfully', async () => {
      // ARRANGE
      const command = new AddCreditRequestCommand(credit, clientID)

      // ACT
      const result = await sut.execute(command)

      // ASSERT
      expect(mockJobRepository.getTotalToPayForClient).toHaveBeenCalledWith(
        clientID,
      )
      expect(mockProfileRepository.incrementClientBalance).toHaveBeenCalledWith(
        clientID,
        credit,
      )
      expect(result).toBeUndefined()
    })

    it('should not add the credit as its more than 1/4 of pending to pay for', async () => {
      const command = new AddCreditRequestCommand(5000, clientID)

      // ACT
      await expect(sut.execute(command)).rejects.toThrow(BadRequestException)

      // ASSERT
      expect(mockJobRepository.getTotalToPayForClient).toHaveBeenCalledWith(clientID)
      expect(mockProfileRepository.incrementClientBalance).not.toHaveBeenCalled()
    })
  })
})

import { CommandHandler, ICommandHandler } from '@nestjs/cqrs'
import { AddCreditRequestCommand } from '@/application/useCases/addCreditRequestUseCase/addCreditRequest.command'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { BadRequestException, UnprocessableEntityException } from '@nestjs/common'
import { ProfileRepository } from '@/infrastructure/repositories/profile.repository'

@CommandHandler(AddCreditRequestCommand)
export class AddCreditRequestUseCase
  implements ICommandHandler<AddCreditRequestCommand>
{
  constructor(
    private readonly jobRepository: JobRepository,
    private readonly profileRepository: ProfileRepository,
  ) {}

  async execute(command: AddCreditRequestCommand): Promise<void> {
    if (!command.credit || !command.clientID) {
      throw new BadRequestException()
    }

    const clientPaymentInfo = await this.jobRepository.getTotalToPayForClient(
      command.clientID,
    )

    if (!clientPaymentInfo) {
      throw new UnprocessableEntityException()
    }

    const amountPendingClient =
      clientPaymentInfo['contract.total_pending_payment']

    if (command.credit > amountPendingClient / 4) {
      throw new BadRequestException(
        'Cannot add more credit than the quarter of pending payment amount',
      )
    }

    await this.profileRepository.incrementClientBalance(
      command.clientID,
      command.credit,
    )

    return
  }
}

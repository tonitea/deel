import { createMock } from '@golevelup/ts-jest'
import { ContractRepository } from '@/infrastructure/repositories/contract.repository'
import { testContractFactory } from '../../../../test/factories/testContract.factory'
import {
  GetContractsByProfileUseCase,
} from '@/application/useCases/getContractsByProfileUseCase/getContractsByProfile.useCase'
import {
  GetContractsByProfileQuery,
} from '@/application/useCases/getContractsByProfileUseCase/getContractsByProfile.query'

describe('GetContractsByProfileUseCase', () => {
  let sut: GetContractsByProfileUseCase

  const profileID = 1

  const mockContractRepository = createMock<ContractRepository>({
    findByProfileID: jest.fn().mockResolvedValue(testContractFactory),
  })

  beforeEach(() => {
    jest.clearAllMocks()

    sut = new GetContractsByProfileUseCase(mockContractRepository)
  })

  describe('execute', () => {
    it('should get contract by ID successfully', async () => {
      // ARRANGE
      const query = new GetContractsByProfileQuery(profileID)

      // ACT
      const result = await sut.execute(query)

      // ASSERT
      expect(mockContractRepository.findByProfileID).toHaveBeenCalledWith(profileID)
      expect(result).toStrictEqual(testContractFactory)
    })
  })
})

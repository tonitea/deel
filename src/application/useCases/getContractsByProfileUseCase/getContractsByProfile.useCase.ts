import { GetContractsByProfileQuery } from '@/application/useCases/getContractsByProfileUseCase/getContractsByProfile.query'
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { BadRequestException, Logger } from '@nestjs/common'
import { ContractRepository } from '@/infrastructure/repositories/contract.repository'
import { ContractEntity } from '@entities/contract/contract.entity'

@QueryHandler(GetContractsByProfileQuery)
export class GetContractsByProfileUseCase
  implements IQueryHandler<GetContractsByProfileQuery>
{
  constructor(private readonly contractRepository: ContractRepository) {}

  private readonly logger = new Logger(GetContractsByProfileUseCase.name)

  async execute(query: GetContractsByProfileQuery): Promise<ContractEntity[]> {
    this.logger.log(
      `GetContractsByProfileUseCase for profile: ${query.profileID}`,
    )

    if (!query.profileID) {
      throw new BadRequestException()
    }

    return await this.contractRepository.findByProfileID(query.profileID)
  }
}

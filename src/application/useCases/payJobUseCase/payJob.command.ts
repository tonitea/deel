export class PayJobCommand {
  constructor(
    public readonly profileID: number,
    public readonly jobID: number,
  ) {}
}

import { createMock } from '@golevelup/ts-jest'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { testJobFactory } from '../../../../test/factories/testJob.factory'
import { PayJobUseCase } from '@/application/useCases/payJobUseCase/payJob.useCase'
import { ProfileRepository } from '@/infrastructure/repositories/profile.repository'
import { PayJobCommand } from '@/application/useCases/payJobUseCase/payJob.command'
import { UnprocessableEntityException } from '@nestjs/common'

describe('PayJobUseCase', () => {
  let sut: PayJobUseCase

  const profileID = 1
  const jobID = 5

  const mockJobRepository = createMock<JobRepository>({
    payJob: jest.fn().mockResolvedValue({}),
    searchJob: jest.fn().mockResolvedValue(testJobFactory),
  })


  const mockProfileRepository = createMock<ProfileRepository>({
    updateBalance: jest.fn().mockResolvedValue({}),
  })

  beforeEach(() => {
    jest.clearAllMocks()

    sut = new PayJobUseCase(mockJobRepository, mockProfileRepository)
  })

  describe('execute', () => {
    it('should pay the job successfully', async () => {
      // ARRANGE
      const command = new PayJobCommand(profileID, jobID)

      // ACT
      await sut.execute(command)

      // ASSERT
      expect(mockJobRepository.searchJob).toHaveBeenCalledWith(profileID, jobID)
      expect(mockJobRepository.payJob).toHaveBeenCalledWith(jobID)
      expect(mockProfileRepository.updateBalance).toHaveBeenCalledWith(
        profileID,
        testJobFactory.contract.client.balance - testJobFactory.price,
      )
    })

    it('should not pay the job because of insufficient balance', async () => {
      testJobFactory.price = 1200000

      const mockJobRepository = createMock<JobRepository>({
        payJob: jest.fn().mockResolvedValue({}),
        searchJob: jest.fn().mockResolvedValue(testJobFactory),
      })

      sut = new PayJobUseCase(mockJobRepository, mockProfileRepository)

      const command = new PayJobCommand(profileID, jobID)

      // ACT
      await expect(sut.execute(command)).rejects.toThrow(UnprocessableEntityException)

      // ASSERT
      expect(mockJobRepository.searchJob).toHaveBeenCalledWith(profileID, jobID)
      expect(mockJobRepository.payJob).not.toHaveBeenCalled()
      expect(mockProfileRepository.updateBalance).not.toHaveBeenCalled()
    })
  })
})

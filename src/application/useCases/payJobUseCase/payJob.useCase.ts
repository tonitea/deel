import { CommandHandler, ICommandHandler } from '@nestjs/cqrs'
import {
  BadRequestException,
  HttpExceptionOptions,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common'
import { PayJobCommand } from '@/application/useCases/payJobUseCase/payJob.command'
import { JobRepository } from '@/infrastructure/repositories/job.repository'
import { ProfileRepository } from '@/infrastructure/repositories/profile.repository'

@CommandHandler(PayJobCommand)
export class PayJobUseCase implements ICommandHandler<PayJobCommand> {
  constructor(
    private readonly jobRepository: JobRepository,
    private readonly profileRepository: ProfileRepository,
  ) {}

  private readonly logger = new Logger(PayJobUseCase.name)

  async execute(command: PayJobCommand): Promise<HttpExceptionOptions | void> {
    this.logger.log(
      `PayJobUseCase for job: ${command.jobID} & client: ${command.profileID}`,
    )

    if (!command.profileID || !command.jobID) {
      throw new BadRequestException()
    }

    const jobClientInfo = await this.jobRepository.searchJob(
      command.profileID,
      command.jobID,
    )

    if (!jobClientInfo) {
      return new BadRequestException(
        'Client or job not found, or job already paid',
      )
    }

    if (jobClientInfo?.contract?.client.balance < jobClientInfo?.price) {
      throw new UnprocessableEntityException('Insufficient balance')
    }

    await this.jobRepository.payJob(command.jobID)

    await this.profileRepository.updateBalance(
      command.profileID,
      +jobClientInfo?.contract?.client?.balance - +jobClientInfo?.price,
    )
  }
}

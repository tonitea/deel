import { IQueryHandler, QueryHandler } from '@nestjs/cqrs'
import { BadRequestException, Logger } from '@nestjs/common'
import { ContractRepository } from '@/infrastructure/repositories/contract.repository'
import { ContractEntity } from '@entities/contract/contract.entity'
import { GetContractByIDQuery } from '@/application/useCases/getContractByIDUseCase/getContractByID.query'

@QueryHandler(GetContractByIDQuery)
export class GetContractByIDUseCase
  implements IQueryHandler<GetContractByIDQuery>
{
  constructor(private readonly contractRepository: ContractRepository) {}

  private readonly logger = new Logger(GetContractByIDUseCase.name)

  async execute(query: GetContractByIDQuery): Promise<ContractEntity[]> {
    this.logger.log(
      `GetContractByIDUseCase for profile: ${query.profileID} & contract: ${query.contractID}`,
    )

    if (!query.profileID || !query.contractID) {
      throw new BadRequestException()
    }

    return await this.contractRepository.findByID(
      query.profileID,
      query.contractID,
    )
  }
}

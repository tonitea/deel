export class GetContractByIDQuery {
  constructor(
    public readonly profileID: number,
    public readonly contractID: number,
  ) {}
}

import { createMock } from '@golevelup/ts-jest'
import { GetContractByIDUseCase } from '@/application/useCases/getContractByIDUseCase/getContractByID.useCase'
import { ContractRepository } from '@/infrastructure/repositories/contract.repository'
import { GetContractByIDQuery } from '@/application/useCases/getContractByIDUseCase/getContractByID.query'
import { testContractFactory } from '../../../../test/factories/testContract.factory'

describe('GetContractByIDUseCase', () => {
  let sut: GetContractByIDUseCase

  const profileID = 1
  const contractorID = 2

  const mockContractRepository = createMock<ContractRepository>({
    findByID: jest.fn().mockResolvedValue(testContractFactory),
  })

  beforeEach(() => {
    jest.clearAllMocks()

    sut = new GetContractByIDUseCase(mockContractRepository)
  })

  describe('execute', () => {
    it('should get contract by ID successfully', async () => {
      // ARRANGE
      const query = new GetContractByIDQuery(profileID, contractorID)

      // ACT
      const result = await sut.execute(query)

      // ASSERT
      expect(mockContractRepository.findByID).toHaveBeenCalledWith(profileID, contractorID)
      expect(result).toStrictEqual(testContractFactory)
    })
  })
})

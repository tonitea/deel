import { Module } from '@nestjs/common'
import { InfrastructureModule } from '@/infrastructure/infrastructure.module'
import { GetContractsByProfileUseCase } from '@/application/useCases/getContractsByProfileUseCase/getContractsByProfile.useCase'
import { GetUnpaidJobsUseCase } from '@/application/useCases/getUnpaidJobsUseCase/getUnpaidJobs.useCase'
import { GetContractByIDUseCase } from '@/application/useCases/getContractByIDUseCase/getContractByID.useCase'
import { PayJobUseCase } from '@/application/useCases/payJobUseCase/payJob.useCase'
import { AddCreditRequestUseCase } from '@/application/useCases/addCreditRequestUseCase/addCreditRequest.useCase'
import { GetBestProfessionUseCase } from '@/application/useCases/getBestProfessionUseCase/GetBestProfession.useCase'
import { GetBestClientsUseCase } from '@/application/useCases/getBestClientsUseCase/getBestClients.useCase'

const useCases = [
  GetContractsByProfileUseCase,
  GetContractByIDUseCase,
  GetUnpaidJobsUseCase,
  PayJobUseCase,
  AddCreditRequestUseCase,
  GetBestProfessionUseCase,
  GetBestClientsUseCase,
]

@Module({
  imports: [InfrastructureModule],
  providers: [...useCases],
  exports: [...useCases],
})
export class ApplicationModule {}
